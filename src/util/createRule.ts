import { ESLintUtils } from '@typescript-eslint/experimental-utils'

export const createRule = ESLintUtils.RuleCreator(
  () => 'https://gitlab.com/brandonkal/csstype/blob/master/README.md'
)
