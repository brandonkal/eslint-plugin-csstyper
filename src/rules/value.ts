import ts from 'typescript'
import { getParserServices } from '../util/getParserServices'
import { createRule } from '../util/createRule'
import csstree from 'css-tree'
import { cartesianArray as cartesian } from 'fast-cartesian'
const syntax = csstree.lexer

type Options = [{ elementUnits: boolean }]

export default createRule<Options, 'csstyper' | 'noObjValue' | 'noDoubleFn'>({
  name: 'value',
  meta: {
    docs: {
      description: 'Warns if a CSS property is invalid',
      category: 'Possible Errors',
      recommended: false,
    },
    messages: {
      csstyper: 'Type `{{reportType}}` is invalid for `{{property}}`',
      noObjValue: 'Unexpected object as CSS value',
      noDoubleFn: 'Unexpected double function',
    },
    schema: [
      {
        elementUnits: {
          type: 'boolean',
        },
      },
    ],
    type: 'problem',
  },
  defaultOptions: [{ elementUnits: true }],
  create(context, [{ elementUnits: allowElementUnits }]) {
    const parserServices = getParserServices(context)
    const checker: ts.TypeChecker = parserServices.program.getTypeChecker()
    /** propRe return groups:
     * 1. Property
     * 2. Value prefix (i.e. if expression is one of a shorthand value).
     * 3. The Template Opener
     */
    const propRe = /\b([A-Za-z-]+)[ \t]*:([^;]*)$/
    // Comments are stripped for locating properties
    const commentRe = /(^|\/\*)[^]*?\*\//g
    const reFn = /^\([^\(]*?\) => /
    const isStyled = node =>
      (node.tag.type === 'CallExpression' &&
        node.tag.callee.type === 'Identifier' &&
        node.tag.callee.name === 'styled') ||
      (node.tag.type === 'MemberExpression' &&
        node.tag.object.type === 'Identifier' &&
        node.tag.object.name === 'styled') ||
      (node.tag.type === 'Identifier' &&
        (node.tag.name === 'css' ||
          node.tag.name === 'injectGlobal' ||
          node.tag.name === 'createGlobalStyle'))
    // Return the visitor for eslint
    return {
      TaggedTemplateExpression(node) {
        if (node.quasi.expressions.length && isStyled(node)) {
          // Let's init TS now. We will check for arguments of the tagged template
          // Get tNode as quasi
          const tsNode: ts.TemplateExpression = parserServices.esTreeNodeToTSNodeMap.get(
            node.quasi
          )
          // Let's figure out how to only use head and tail...
          if (tsNode.templateSpans) {
            // Loop through all children, not just the template spans...
            let prefix = ''
            let suffix = ''
            let collected: {
              prefix: string
              typeString: string
              types: string[]
              suffix: string
              isObj: boolean
              isFn: boolean
              tsExpNode: ts.Node
              skip: boolean
            }[] = []
            tsNode.forEachChild(child => {
              if (ts.isTemplateHead(child)) {
                prefix = child.text
                return
              }
              if (ts.isTemplateSpan(child)) {
                let skip = false
                // Check if we should use the previous suffix as the prefix
                if (collected.length) {
                  prefix = collected[collected.length - 1].suffix
                }
                suffix = child.literal.getText()
                let exp = child.expression
                let tAt = checker.getTypeAtLocation(exp)
                let isObj =
                  !(tAt.getCallSignatures().length > 0) &&
                  tAt.getFlags() === ts.TypeFlags.Object
                let tStr = checker.typeToString(tAt)
                if (isObj) {
                  skip = true
                }
                // Enumerate possible values from type:
                // Remove the first function. If there exists another one, we will return
                let isFn = false
                let types: string[] = []
                if (!skip) {
                  tStr = tStr.replace(reFn, '')
                  if (tStr === 'any') {
                    skip = true
                  } else if (reFn.test(tStr)) {
                    skip = true
                    isFn = true
                  } else {
                    types = tStr
                      .split(' | ')
                      .filter(t => t !== 'string')
                      .map(type =>
                        type === 'number' ? '1' : type.replace(/^"|"$/g, '')
                      )
                  }
                }
                if (!types.length) {
                  skip = true
                }
                collected.push({
                  prefix,
                  typeString: tStr,
                  types,
                  suffix,
                  isObj,
                  isFn,
                  tsExpNode: exp,
                  skip,
                })
              }
            })
            // A collection of results to verify with CSS Tree
            interface Declaration {
              values: string[][]
              property?: string
              tsExpNode?: ts.Node
            }
            let declarations: Declaration[] = []
            // Metadata for collection.
            let building = false
            let skip = false
            let current: Declaration = {
              values: [],
              property: '',
            }

            /**
             * Loop through all collected expressions.
             * We keep track if we are building. If not, we initialize a new object
             * with the extracted property, prefix, and tsNode.
             * Build a `string[][]` containing the parts to construct a property
             * This includes types and plain strings. If we come across an end marker,
             * we set building to false and push this object to the final array.
             */
            collected.forEach(expression => {
              if (!building) {
                // Reset current object
                current = {
                  property: undefined,
                  values: [],
                }
                let strippedPrefix = expression.prefix.replace(commentRe, '')
                let results = propRe.exec(strippedPrefix)
                if (results && results.length) {
                  let property = results[1]
                  let valuePrefix = results[2]
                    ? results[2].replace(/\${$/, '')
                    : ''
                  current = {
                    property,
                    values: [[valuePrefix]],
                  }
                }
              }
              // Match until semi OR newline (prettier will typically handle ASI)
              // We continue building if skip is true, but we don't report it.
              let split = expression.suffix.split(/[;\n`]/, 2)
              let middle = split[0].endsWith('${')
              let isDeclaration = split[0].endsWith('{') && !middle
              if (isDeclaration) {
                building = false
                return
              }
              // Objects in value position are incorrect
              if ((expression.isObj && current.property) || expression.isFn) {
                let esNode = parserServices.tsNodeToESTreeNodeMap.get(
                  expression.tsExpNode as any
                )
                const messageId = expression.isObj ? 'noObjValue' : 'noDoubleFn'
                context.report({
                  node: esNode,
                  messageId,
                })
                skip = true
              }
              if (expression.skip) {
                skip = true
              }
              if (middle) {
                split[0] = split[0].replace(/\${$/, '')
                building = true
              } else {
                building = false
              }
              let valueMatch = split[0].replace(/^}/, '')
              let valueSuffix = valueMatch || ''
              current.values.push(expression.types)
              current.values.push([valueSuffix])
              if (!building) {
                if (!skip) {
                  current.tsExpNode = expression.tsExpNode
                  declarations.push(current)
                }
                skip = false
              }
            })

            /**
             * Loop through all discovered declarations,
             * parse and validate values with CSSTree, and report errors.
             */
            declarations.forEach(decl => {
              let property = decl.property
              if (!property) {
                return
              }
              // Build all possible value combinations
              let values = cartesian(...decl.values).map(parts => {
                let v = parts.join('').trim()
                v = v.replace(/!important$/, '').trim()
                return allowElementUnits
                  ? v.replace(/(\d+)(emin|emax|eh|ew)\b/g, (_, num, unit) => {
                      return `calc(${num} * var(--${unit}))`
                    })
                  : v
              })
              interface Invalid {
                property: string
                values: string[]
              }
              let invalid: Invalid = {
                property,
                values: [],
              }

              // Keep track of building a string. i.e. if a template doesn't end a string
              values.forEach(value => {
                let parsedValue
                // Now let's parse with CSSTree
                try {
                  parsedValue = csstree.parse(value, {
                    context: 'value',
                    property,
                  })
                } catch (e) {
                  // ignore for now
                }
                if (parsedValue === undefined || parsedValue === null) {
                  return
                }
                // Validate each parsed value:
                let match = syntax.matchProperty(property, parsedValue)
                let error = match.error
                if (error) {
                  let msg = error.rawMessage || error.message || error
                  // ignore unknown property errors
                  if (error.name === 'SyntaxReferenceError') {
                    return
                  }
                  if (msg === 'Mismatch') {
                    invalid.values.push(value)
                  }
                }
              })
              // Loop through invalid values and build an error message
              if (invalid.values.length) {
                let reportType = invalid.values.join(' | ')
                let esNode = parserServices.tsNodeToESTreeNodeMap.get(
                  decl.tsExpNode as any
                )
                context.report({
                  node: esNode,
                  messageId: 'csstyper',
                  data: {
                    property,
                    reportType,
                  },
                })
              }
            })
          }
        }
      },
    }
  },
})
