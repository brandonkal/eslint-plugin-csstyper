import path from 'path'
import rule from '../../src/rules/value'
import { RuleTester } from '../RuleTester'

const rootDir = path.resolve(__dirname, '../fixtures/')
const ruleTester = new RuleTester({
  parserOptions: {
    ecmaVersion: 2015,
    tsconfigRootDir: rootDir,
    project: './tsconfig.json',
  },
  parser: '@typescript-eslint/parser',
})

ruleTester.run('csstyper', rule, {
  valid: [
    "const bar = 3; const Span = styled('span')`font-size: ${bar}px`",
    'const bar = 3; const Span = styled.span`font-size: ${bar}px`',
    "const bar = '3em'; const Span = styled.span`font-size: ${bar}`",
    'const bar = 3; const Span = css`font-size: ${bar}px`',
    "const bar = '3em'; const Span = css`font-size: ${bar}`",
    "const sortBg = '#fff'; const Span = css`background: ${sortBg} !important;`",
    "const sortBg = '#fff'; const Span = styled`background: ${sortBg} !important;`",
    "const bar = '3emax'; const Span = styled.span`font-size: ${bar}`",
    "const bar = '3'; const Span = styled.span`font-size: ${bar}ew`",
    "const bar = '3em'; const Not = notStyled('span')`font-size: ${bar}px`",
    "const bar = '3em'; const Not = notStyled.span`font-size: ${bar}px`",

    `// Ignored as type return type is any... - valid
      const theme = { big: '10em', small: '5em' } as const
      interface Props { big?: typeof theme.big }
      const Btn = (props => styled('button')<Props>\`
        font-size: ${'${props => props.big && props.big || theme.small}'}px;
      \`)({} as Props)`,
    `// Custom type alias - valid
      type mytype = {}
      const something: mytype = {}
      export const Div = styled('div')\`
        ${'${something}'}
      \`
    `,
    `// Advanced custom type alias - valid
      type CSSProperties = {
        [key: string]: string | number | CSSProperties;
      };

      const Custom: CSSProperties = {
          background: 'red'
      }
      export const Div = styled('div')\`
        background: red;
        ${'${Custom}'}
      \`
    `,

    `// Object in declaration position - valid
      export const Div = styled('div')\`
        ${'${{ backgroundColor: pink }}'};
      \`
    `,
    `// Shorthand Property - valid
      export const Div = (props => styled('div')\`
        background: url(images/bg.gif) ${'${"no-repeat"}'} ${'${"left"}'} top;
      \`)({} as Props)
    `,
    `// Referenced Function with known return type... - valid
      const theme = {
        big: '10em',
        small: '5em'
      } as const
      const fn = (props: Props) => props.big && props.big || theme.small

      interface Props {
        big?: '10em'
      }

      export const Div = (props => styled('div')<Props>\`
        color: red;
        size: ${'${16}'};
        height: ${'${fn}'};
        width: ${'${props.big || theme.small || 20}'};
      \`)({} as Props)
    `,
    `// Complex Terenary - valid (skipped | typof backPos == string)
      let isPrimary = false
      let backPos = isPrimary ? "left" : 100 > 1 ? "right" : "white"
      export const Div = styled('div')\`
        background: 5% / 15% ${'${120 / 2}'}% ${'${backPos}'} repeat-x url("star.png");
      \`
    `,
    `// Complex Terenary - valid - corrected
      let isPrimary = false
      const backPos = isPrimary ? "left" : 100 > 1 ? "right" : "white"
      export const Div = styled('div')\`
        background: ${'${backPos}'} 5% / 15% ${'${120 / 2}'}% repeat-x url("star.png");
      \`
    `,
    `// Array expression as selector - valid
      type BoxProps = {
        disabled?: boolean
        checked?: boolean
      }

      export const Box = ((props: BoxProps) => styled('div')\`
        height: ${'${(props: BoxProps) => (props.disabled ? 10 : 20)}'}px;
        &${'${[props.disabled]}'} {
          background: red;
        }
      \`)({} as BoxProps)
    `,
    `// Array expression as selector 2 - valid
      type BoxProps = {
        disabled?: boolean
        checked?: boolean
      }
      export const BoxBase = (props => styled.span<BoxProps>\`
        height: \${(props: BoxProps) => (props.disabled ? 10 : 20)}px;
        line-height: 1;
        \${[props.disabled]} {
          color: red;
        }

        &:hover \${Inner} {
          border-color: #1890ff;
        }
      \`)({} as BoxProps)
    `,
    `// Styled Component Selector - valid
      const Inner = {
        '_styled': true
      } as const
      export const Div = styled('div')\`
        height: ${'${(props: BoxProps) => (props.disabled ? 0 : "10px")}'};
        color: red;
        background: black;
        .div::hover ${'${Inner}'} {
          padding: 10px;
        }
      \`
    `,
    `// Large Component - valid
    type BoxProps = {
      disabled?: boolean
      checked?: boolean
    }

    const Inner = {} as const
    const Input = {} as const

    function styled() {
      return function(...args: any) {}
    }
    styled.span = function<T>(...args: any) {}

    export const BoxBase = (props => styled.span<BoxProps>\`
      height: \${(props: BoxProps) => (props.disabled ? 10 : 20)}px;
      line-height: 1;

      &:hover \${Inner} {
        border-color: #1890ff;
      }

      &:hover::after {
        visibility: visible;
      }

      & + span {
        padding-left: 8px;
      }

      &\${[props.disabled]} {
        cursor: not-allowed;
        \${Input} {
          cursor: not-allowed;
        }
        & + span {
          cursor: not-allowed;
        }
      }

      &\${[props.checked]}::after {
        @keyframes checkEffect {
          from {
            transform: scale(1);
            opacity: 0.5;
          }
          to {
            transform: scale(1.6);
            opacity: 0;
          }
        }
      }
    \`)({} as BoxProps)
    `,
    `// filterProps -- comment with colon - valid
    const Avatar = (p => styled.span\`
      /* Do: Comment */
      \${{ filterProps: ({ name, ...o }) => o }}
      box-sizing: border-box;
    \`)({})`,
    `
    const sortBg = '#f5f5f5'
    function Nested() {
      return cx(
        sortedColumn.column === col.key &&
          css\`
            background: \${sortBg} !important;
          \`
      )
    }`,
  ],
  // Uncomment below and remove '--' to filter
  // ].filter(v => v.includes('Nested')),
  // @--ts-ignore
  invalid: [
    {
      code: "const bar = '3em'; const Not = styled.span`font-size: ${bar}px`",
      errors: [{ messageId: 'csstyper', line: 1, column: 57 }],
    },
    {
      code: `// Custom type alias - invalid
      type mytype = {}
      const something: mytype = {}
      export const Div = styled('div')\`
        background: ${'${something}'}
      \`
    `,
      errors: [
        {
          messageId: 'noObjValue',
          line: 5,
          column: 23,
        },
      ],
    },
    {
      code: `// Terenary - invalid
      let isPrimary = false
      export const Div = styled('div')\`
        background: ${'${isPrimary ? "red" : 100 > 1 ? "redder" : "white"}'}
      \`
    `,
      errors: [
        {
          messageId: 'csstyper',
          line: 4,
          column: 23,
        },
      ],
    },
    {
      code: `// Complex Terenary - invalid
      let isPrimary = false
      const backPos = isPrimary ? "left" : 100 > 1 ? "right" : "white"
      export const Div = styled('div')\`
        background: 5% / 15% ${'${120 / 2}'}% ${'${backPos}'} repeat-x url("star.png");
      \`
    `,
      errors: [
        {
          messageId: 'csstyper',
          line: 5,
          column: 44,
        },
      ],
    },
    {
      code: `// Advanced custom type alias - invalid
      type CSSProperties = {
        [key: string]: string | number | CSSProperties;
      };

      const Custom: CSSProperties = {
          background: 'red'
      }
      export const Div = styled('div')\`
        background: ${'${Custom}'}
      \`
    `,
      errors: [
        {
          messageId: 'noObjValue',
          line: 10,
          column: 23,
        },
      ],
    },
    {
      code: `// Double Wrapped Function should throw... - invalid
      const theme = {
        big: '10em',
        small: '5em'
      } as const
      const fn = (props: Props) => props.big && props.big || theme.small

      interface Props {
        big?: '10em'
      }

      export const Div = (props => styled('div')<Props>\`
        height: ${'${() => fn}'};
      \`)({} as Props)
    `,
      errors: [
        {
          messageId: 'noDoubleFn',
          line: 13,
          column: 19,
        },
      ],
    },
    {
      code: `// Object in rule position - invalid
      export const Div = styled('div')\`
        color: ${'${{ backgroundColor: pink }}'};
      \`
    `,
      errors: [
        {
          messageId: 'noObjValue',
          line: 3,
          column: 18,
        },
      ],
    },
    {
      code: `// Object in rule position reference - invalid
      const obj = { backgroundColor: 'pink' }
      type mytype = {}
      const something: mytype = {}
      export const Div = styled('div')\`
        color: ${'${obj}'};
        background: ${'${something}'}
      \`
    `,
      errors: [
        {
          messageId: 'noObjValue',
          line: 6,
          column: 18,
        },
        {
          messageId: 'noObjValue',
          line: 7,
          column: 23,
        },
      ],
    },
    // Element Units when disallowed
    {
      code: "const bar = '3'; const Not = styled.span`font-size: ${bar}ew`",
      options: [{ elementUnits: false }],
      errors: [
        {
          messageId: 'csstyper',
          line: 1,
          column: 55,
        },
      ],
    },
    {
      code: "const bar = '3emax'; const Not = styled.span`font-size: ${bar}`",
      options: [{ elementUnits: false }],
      errors: [
        {
          messageId: 'csstyper',
          line: 1,
          column: 59,
        },
      ],
    },
  ],
  // Uncomment line below to filter
  // ].filter((_, i) => i === 0),
})
