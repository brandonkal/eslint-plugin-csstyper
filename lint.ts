/* stylelint-disable */
function styledFn(tag: string) {
  return {}
}

const styled = styledFn as CreateStyled

export type CSSProperties = {
  [key: string]: string | number | CSSProperties
}

type _isStyled = { __linaria: true }

export type StyledComponent<Tag, ExtraProps> = {
  tag: Tag
  props: ExtraProps
} & _isStyled

// The tagged template function
type StyledTag<Tag> = <ExtraProps = {}>(
  strings: TemplateStringsArray,
  ...exprs: Array<
    | string
    | number
    | CSSProperties
    // Strictly typing props argument would break the generated StyledComponent.
    | ((props: any) => string | number)
    | [unknown] // Modifier selectors
    | Tag & ExtraProps
  >
) => StyledComponent<Tag, ExtraProps>

type JSXInEl = {
  span: any
  div: any
}

// The main styled constructor function
export type CreateStyled = {
  readonly [key in keyof JSXInEl]: StyledTag<key>
} & {
  <Tag extends keyof JSXInEl>(tag: Tag): StyledTag<Tag>
}

////// USAGE  //////

const theme = {
  big: '10em',
  small: '5em',
} as const

const fn = (props: Props) => (props.big && props.big) || theme.small

interface Props {
  big?: '10em'
}

export const Div = (props => styled('div')<Props>`
  color: red;
  /* stylelint-disable */
  height: ${fn};
  /* This should cause an eslint error */
  width: ${props.big || theme.small}px;
`)({} as Props)
