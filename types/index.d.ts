declare module 'fast-cartesian' {
  export const cartesianArray: (...args: string[][]) => string[][]
}
